
#lsetup "root 6.04.02-x86_64-slc6-gcc48-opt"
setupATLAS

lsetup "root 6.12.04-x86_64-slc6-gcc62-opt"
lsetup "sft releases/doxygen/1.8.11-68a7c"
lsetup cmake

export LD_LIBRARY_PATH=${ROOTSYS}/lib:${LD_LIBRARY_PATH}
export PATH=${ROOTSYS}/bin:${PATH}
export PATH=${PATH}:/${PWD}/build
# export PATH=/afs/cern.ch/sw/lcg/external/doxygen/1.8.2/x86_64-slc6-gcc48-opt/bin:$PATH
export PYTHONPATH=${ROOTSYS}/lib:${PYTHONDIR}/lib:${PYTHONPATH}

