
#ifndef __XMLREADER__
#define __XMLREADER__

#include <boost/property_tree/xml_parser.hpp>
#include <iostream>

namespace LQ
{
    class XMLReader
    {
        public:
            XMLReader(std::string input){std::cout << input << std::endl; m_config = input; Read("", "");};
            XMLReader(std::string input, std::string mc, std::string mass){std::cout << input << std::endl; m_config = input; Read(mc, mass);};
            ~XMLReader();
            void Read(std::string,std::string);
            std::string getJobName()                 {return m_job_name;}
            std::string getWeightFile()              {return m_weight;}
            std::string getOutputFile()              {return m_output;}
            std::string getInputDir()                {return m_input_dir;}
            std::string getSignal()                  {return m_signal;}
            std::string getSignalTree()              {return m_signal_tree;}
            std::string getBackground()              {return m_background;}
            std::string getBackgroundTree()          {return m_background_tree;}
            std::string getTestCut()                 {return m_cut_test;}
            std::string getTrainCut()                {return m_cut_train;}
            std::string getSignalCut()               {return m_cut_signal;}
            std::string getBackgroundCut()           {return m_cut_background;}
            std::vector<std::string> getInputVarVec(){return m_inputVar;}
            std::string getInputVarAt(int index)     {return m_inputVar.at(index);}
            std::string getFactoryOption()           {return m_factory_option;}
            std::string getBDTOption()               {return m_bdt_option;}

        private:
            std::string m_config;

            std::string m_job_name;
            std::string m_weight;
            std::string m_output;
            std::string m_input_dir;
            std::string m_signal;
            std::string m_signal_tree;
            std::string m_background;
            std::string m_background_tree;
            std::string m_cut_test;
            std::string m_cut_train;
            std::string m_cut_signal;
            std::string m_cut_background;
            std::vector<std::string> m_inputVar;
            std::string m_factory_option;
            std::string m_bdt_option;
    };
}

#endif
