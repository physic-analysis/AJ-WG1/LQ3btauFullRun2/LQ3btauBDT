#ifndef __LQ3BTAUBDT__
#define __LQ3BTAUBDT__

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <sstream>

//#include <TChain.h>
#include <TCut.h>
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TObject.h>
//#include <TVersionCheck.h>
//#include <TObjString.h>
//#include <TSystem.h>
//#include <TROOT.h>
//#include <TPluginManager.h>
//#include <TMath.h>

#include <TMVA/Factory.h>
#include <TMVA/DataLoader.h>
//#include <TMVA/Tools.h>
#include <TMVA/Config.h>

#include "XMLReader.h"

namespace LQ
{
    class LQ3btauBDT 
    {
        public:
            LQ3btauBDT() = delete;
            LQ3btauBDT(std::string config_xml);
            LQ3btauBDT(std::string config_xml, std::string mc, std::string mass, std::string Btag, std::string AorB, std::string SelectionName);
            LQ3btauBDT(std::string config_xml, std::map<TString, TString> bdt_option);
            ~LQ3btauBDT();
            void RunBDT();
            TMVA::Factory* getFactory();

        private:
            void CreateFactory();
            void SetInputFiles();
            void SetInputVars();
            void SetTTrees();
            void SetCut();
            void AddTree();
            void EventWeight();
            void PrepareTrainingAndTestTree();
            void BookMethod();
            void Executing();

            std::map<TString, TFile*> m_input_files;
            std::map<TString, TTree*> m_input_trees;
            TString m_output_dir;
            TString m_job_name;
            TFile*  m_output_file;
            TString m_output_weight;
            TString m_analysis;
            TCut    m_cut_sig;
            TCut    m_cut_bkg;
            TCut    m_cut_test;
            TCut    m_cut_train;
            TString m_bdt_option;
            TString m_factory_option;
            TMVA::Factory*    m_factory;
            TMVA::DataLoader* m_dataloader;
            XMLReader m_xml;
    };
}
#endif
