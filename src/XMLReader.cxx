
#include "XMLReader.h"
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>

namespace LQ
{
    XMLReader::~XMLReader()
    {}

    void XMLReader::Read(std::string mc, std::string mass)
    {
        boost::property_tree::ptree pt;
        boost::property_tree::xml_parser::read_xml( m_config, pt);
        
        if (boost::optional<std::string> xml_attr = pt.get_optional<std::string>("LQ3btauConfig.JobName"))
            m_job_name = xml_attr.get();
        
        if (boost::optional<std::string> xml_attr = pt.get_optional<std::string>("LQ3btauConfig.OutputFile"))
            m_output = xml_attr.get();
        
        if (boost::optional<std::string> xml_attr = pt.get_optional<std::string>("LQ3btauConfig.WeightFile"))
            m_weight = xml_attr.get();

        if (boost::optional<std::string> xml_attr = pt.get_optional<std::string>("LQ3btauConfig.InputFiles.Dir"))
            m_input_dir = xml_attr.get();

        for (auto it : pt.get_child("LQ3btauConfig.InputFiles")) {
            // choose mc16a,d,e
            if (auto xml_mc = it.second.get_optional<std::string>("<xmlattr>.mc")){
                if ( mc != xml_mc ) continue;
                std::cout << "MC campagin = " << xml_mc.get() << std::endl;
                std::cout << mass << std::endl;
                
                // Signal
                for (auto it : pt.get_child("LQ3btauConfig.InputFiles.Period")) {
                    if (auto xml_mass = it.second.get_optional<std::string>("<xmlattr>.mass")){
                        if ( mass != xml_mass ) continue;
                        if (auto xml_file = it.second.get_optional<std::string>("File")){
                            std::cout << "File name = " << xml_file.get() << std::endl;
                            m_signal = xml_file.get();
                        }
                        if (auto xml_tree = it.second.get_optional<std::string>("TTree")){
                            std::cout << "Tree name = " << xml_tree.get() << std::endl;
                            m_signal_tree = xml_tree.get();
                        }
                    }
                }
                // background
                if (auto xml_attr = it.second.get_optional<std::string>("Background.File")){
                    std::cout << "File name = " << xml_attr.get() << std::endl;
                    m_background = xml_attr.get();
                }
                if (auto xml_attr = it.second.get_optional<std::string>("Background.TTree")){
                    std::cout << "Tree name = " << xml_attr.get() << std::endl;
                    m_background_tree = xml_attr.get();
                }
            }
        }
        
        if (boost::optional<std::string> xml_attr = pt.get_optional<std::string>("LQ3btauConfig.Cut.Test"))
            m_cut_test = xml_attr.get();
        
        if (boost::optional<std::string> xml_attr = pt.get_optional<std::string>("LQ3btauConfig.Cut.Train"))
            m_cut_train = xml_attr.get();
        
        if (boost::optional<std::string> xml_attr = pt.get_optional<std::string>("LQ3btauConfig.Cut.Signal"))
            m_cut_signal = xml_attr.get();
        
        if (boost::optional<std::string> xml_attr = pt.get_optional<std::string>("LQ3btauConfig.Cut.Background"))
            m_cut_background = xml_attr.get();

        BOOST_FOREACH (const boost::property_tree::ptree::value_type& child, pt.get_child("LQ3btauConfig.InputVars")) {
            const std::string value = boost::lexical_cast<std::string>(child.second.data());
            m_inputVar.push_back(value);
        }
        
        if (boost::optional<std::string> fac_opt = pt.get_optional<std::string>("LQ3btauConfig.FactoryOptions"))
            m_factory_option = fac_opt.get();
        
        if (boost::optional<std::string> bdt_opt = pt.get_optional<std::string>("LQ3btauConfig.BdtOptions"))
            m_bdt_option = bdt_opt.get();

        std::cout << "====== XML Reader ======" << std::endl;
        std::cout << "  m_job_name        " << m_job_name        << std::endl;
        std::cout << "  m_output          " << m_output          << std::endl;
        std::cout << "  m_weight          " << m_weight          << std::endl;
        std::cout << "  m_input_dir       " << m_input_dir       << std::endl;
        std::cout << "  m_signal          " << m_signal          << std::endl;
        std::cout << "  m_signal_tree     " << m_signal_tree     << std::endl;
        std::cout << "  m_background      " << m_background      << std::endl;
        std::cout << "  m_background_tree " << m_background_tree << std::endl;
        std::cout << "  m_cut_test        " << m_cut_test        << std::endl;
        std::cout << "  m_cut_train       " << m_cut_train       << std::endl;
        std::cout << "  m_factory_option  " << m_factory_option  << std::endl;
        std::cout << "  m_bdt_option      " << m_bdt_option      << std::endl;
        for ( ssize_t i=0; i< m_inputVar.size(); i++)
            std::cout << m_inputVar.at(i) << std::endl;
        std::cout << "========================" << std::endl;
    }

}
