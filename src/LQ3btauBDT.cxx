
#include "LQ3btauBDT.h"
#include <sys/stat.h>
#include <iostream>
#include <fstream>

namespace LQ
{
    LQ3btauBDT::LQ3btauBDT(std::string config_xml) : 
        m_factory(0),
        m_dataloader(0),
        m_xml("/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/config/" + config_xml, "", "" )
    {
        m_output_file   = new TFile( m_xml.getOutputFile().c_str(), "recreate");
        m_output_weight = m_xml.getWeightFile();
        m_job_name      = m_xml.getJobName();
        m_cut_sig       = m_xml.getSignalCut().c_str();
        m_cut_bkg       = m_xml.getBackgroundCut().c_str();
        m_bdt_option    = m_xml.getBDTOption().c_str();
        m_factory_option= m_xml.getFactoryOption().c_str();
    }
    
    LQ3btauBDT::LQ3btauBDT(std::string config_xml, std::string mc, std::string mass, std::string Btag, std::string AorB, std::string SelectionName) : 
        m_factory(nullptr),
        m_dataloader(nullptr),
        m_xml("/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/config/" + config_xml, mc, mass )
    {
        m_output_weight  = m_xml.getWeightFile() + "_" + mc + "_" + Btag + "_" + AorB + "_" + SelectionName;
        m_job_name       = m_xml.getJobName() + mass;
        m_output_file    = new TFile( m_job_name + "_" + m_output_weight + ".root", "recreate");
        m_cut_sig        = m_xml.getSignalCut().c_str();
        m_cut_bkg        = m_xml.getBackgroundCut().c_str();
        m_bdt_option     = m_xml.getBDTOption().c_str();
        m_factory_option = m_xml.getFactoryOption().c_str();
        
        TString cut_test  = m_xml.getTestCut().c_str();
        TString cut_train = m_xml.getTrainCut().c_str();
        // b-tag selection
        if        ( Btag == "1btag" ) {
            cut_test  += "&& Btag == 1";
            cut_train += "&& Btag == 1";
        } else if ( Btag == "2btag" ) {
            cut_test  += "&& Btag == 2";
            cut_train += "&& Btag == 2";
        }
        // Event/Odd selection
        if        ( AorB == "A" ) {
            cut_test  += "&& isOdd == 1";
            cut_train += "&& isOdd == 0";
        } else if ( AorB == "B" ) {
            cut_test  += "&& isOdd == 0";
            cut_train += "&& isOdd == 1";
        }
        m_cut_test  = cut_test;
        m_cut_train = cut_train;

	// Initialize
	m_input_files["signal"]     = nullptr;
	m_input_files["background"] = nullptr;
	m_input_trees["signal"]     = nullptr;
	m_input_trees["background"] = nullptr;
    }
    
    LQ3btauBDT::LQ3btauBDT(std::string config_xml, std::map<TString, TString> bdt_option) : 
        m_factory(0),
        m_dataloader(0),
        m_xml("/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/config/" + config_xml )
    {
        TString output_file = m_xml.getSignalTree()+ "_NTrees" + bdt_option["NTrees"] + "_MaxDepth" + bdt_option["MaxDepth"] + "_MinNodeSize" + bdt_option["MinNodeSize"]+ "_nCuts" + bdt_option["nCuts"] + "_" + bdt_option["BoostType"];
        std::string output_root = static_cast<std::string>(output_file) + ".root";
        std::cout << "takeda + " << output_file << std::endl;

        m_output_file   = new TFile( output_root.c_str() , "recreate");
        m_output_weight = output_file;
        m_job_name      = m_xml.getJobName();
        m_cut_sig       = m_xml.getSignalCut().c_str();
        m_cut_bkg       = m_xml.getBackgroundCut().c_str();
        m_bdt_option    = ""; // Construct this option using the arguments, bdt_option
        m_factory_option= m_xml.getFactoryOption().c_str();
    
        m_bdt_option  = "!H:!V:";
        m_bdt_option += "NTrees="       + bdt_option["NTrees"]      + ":";
        m_bdt_option += "MaxDepth="     + bdt_option["MaxDepth"]    + ":";
        m_bdt_option += "MinNodeSize="  + bdt_option["MinNodeSize"] + ":";
        m_bdt_option += "nCuts="        + bdt_option["nCuts"]       + ":";
        m_bdt_option += "BoostType="    + bdt_option["BoostType"]   + ":";
        m_bdt_option += "AdaBoostBeta=0.15:"; // AdaBoost specifi option
        
        // if      ( bdt_option["BoostType"] == "AdaBoost" )     { m_bdt_option += "AdaBoostBeta=" + bdt_option["AdaBoostBeta"]+ ":"; }
        // else if ( bdt_option["BoostType"] == "RealAdaBoost" ) { m_bdt_option += "AdaBoostBeta=" + bdt_option["AdaBoostBeta"]+ ":"; }
        // else if ( bdt_option["BoostType"] == "Bagging" )      { m_bdt_option += "AdaBoostBeta=" + bdt_option["AdaBoostBeta"]+ ":"; }
        // else if ( bdt_option["BoostType"] == "AdaBoostR2" )   { m_bdt_option += "AdaBoostBeta=" + bdt_option["AdaBoostBeta"]+ ":"; }
        // else if ( bdt_option["BoostType"] == "Grad" )         { m_bdt_option += "AdaBoostBeta=" + bdt_option["AdaBoostBeta"]+ ":"; }

        m_bdt_option += "SeparationType=GiniIndex:";
        m_bdt_option += "PruneMethod=NoPruning:";

        std::cout << "Commandline defined BDT options : " << std::endl;
        std::cout << m_bdt_option << std::endl;
    }

    LQ3btauBDT::~LQ3btauBDT()
    {
        m_output_file->Close();
        // m_input_files["signal"]    ->Close(); 
        // m_input_files["background"]->Close(); 

        delete m_input_trees["signal"]; 
        delete m_input_trees["background"]; 
        delete m_input_files["signal"]; 
        delete m_input_files["background"]; 

        delete m_output_file;
        delete m_factory;
        delete m_dataloader;
    }

    void LQ3btauBDT::RunBDT()
    {
        std::cout << ">>> CreateFactory : " << std::endl;
        CreateFactory();

        std::cout << ">>> SetInputFiles : " << std::endl;
        SetInputFiles();
        
        std::cout << ">>> SetInputVars : " << std::endl;
        SetInputVars();
        
        std::cout << ">>> SetInputTTrees : " << std::endl;
        SetTTrees();
        
        std::cout << ">>> SetCut : " << std::endl;
        SetCut();
        
        std::cout << ">>> AddTree : " << std::endl;
        AddTree();
        
        std::cout << ">>> EventWeight : " << std::endl;
        EventWeight();
        
        std::cout << ">>> PrepareTrainingAndTestTree : " << std::endl;
        PrepareTrainingAndTestTree();
        
        std::cout << ">>> BookMethod : " << std::endl;
        BookMethod();
        
        std::cout << ">>> Executing : " << std::endl;
        Executing();
    }

    TMVA::Factory* LQ3btauBDT::getFactory()
    {
        return m_factory;
    }

    void LQ3btauBDT::CreateFactory()
    {
        // --- (1) Create factory
        /* third argument is the job option string.
         * V               : verbose flag
         * Silent          : Batch mode
         * Color           : Flag for coloured screen output
         * AnalysisType    : Set the analysis type (Classification, Regression, Multiclass, Auto)
         * Transformations : List of transformations to test
         * */
        TMVA::Factory* factory = new TMVA::Factory( 
                m_job_name,  /* Job name */
                m_output_file, /* output file */
                m_factory_option
                );
        TMVA::DataLoader* dataloader = new TMVA::DataLoader("dataset");

        m_factory = factory;
        m_dataloader = dataloader;
    }

    void LQ3btauBDT::SetInputVars()
    {
        // --- (2) Add variables/targets
        /* Declaration of variables used to train the MVA methods
         * F           : floating point
         * := operator : this defines labels for shorthand notation in screen outputs and plots
         * */
        std::vector<std::string> vec = m_xml.getInputVarVec();
        for ( unsigned int iVar=0; iVar< vec.size(); iVar++ ){
            std::cout << "Add variables : " << vec.at(iVar) << std::endl;
            m_dataloader->AddVariable( vec.at(iVar), 'F' );
        }
    }

    void LQ3btauBDT::SetInputFiles()
    {
        TString dir = m_xml.getInputDir();
        TString sig = m_xml.getSignal();
        TString bkg = m_xml.getBackground();
        m_input_files["signal"]     = new TFile( dir + sig, "read");
        m_input_files["background"] = new TFile( dir + bkg, "read");
    }

    void LQ3btauBDT::SetTTrees()
    {
        m_input_trees["signal"]     = (TTree*)m_input_files["signal"]    ->Get( m_xml.getSignalTree().c_str());
        m_input_trees["background"] = (TTree*)m_input_files["background"]->Get( m_xml.getBackgroundTree().c_str());
    }

    void LQ3btauBDT::SetCut()
    {
        // // b-tag selection
        // if        ( Btag == "1btag" ) {
        //     cut_sig += "&& Btag == 1";
        // std::cout << cut_sig << std::endl;
        //     cut_bkg += "&& Btag == 1";
        // } else if ( Btag == "2btag" ) {
        //     cut_sig += "&& Btag == 2";
        //     cut_bkg += "&& Btag == 2";
        // }
        // // Event/Odd selection
        // if        ( AorB == "A" ) {
        //     cut_sig += "&& EventNumber == 1";
        // std::cout << cut_sig << std::endl;
        //     cut_bkg += "&& EventNumber == 0";
        // } else if ( AorB == "B" ) {
        //     cut_sig += "&& EventNumber == 0";
        //     cut_bkg += "&& EventNumber == 1";
        // }
        // m_cut_sig = cut_sig;
        // m_cut_bkg = cut_bkg;
        // m_cut_test  = m_xml.getTestCut().c_str();
        // m_cut_train = m_xml.getTrainCut().c_str();
    }

    void LQ3btauBDT::AddTree()
    {
        std::cout << "Test Cut : " << m_cut_test  << std::endl;
        std::cout << "Train Cut : " << m_cut_train << std::endl;
        // --- (3) Add trees
        m_dataloader->AddTree( m_input_trees["signal"]     , "Signal"    , 1.0, m_cut_test, "test");
        m_dataloader->AddTree( m_input_trees["background"] , "Background", 1.0, m_cut_test, "test"); 
        m_dataloader->AddTree( m_input_trees["signal"]     , "Signal"    , 1.0, m_cut_train, "train");
        m_dataloader->AddTree( m_input_trees["background"] , "Background", 1.0, m_cut_train, "train"); 
    }


    void LQ3btauBDT::EventWeight()
    {
        // --- (4) Event weight
        /* Indivisual event weights can be applied.
         * */
        m_dataloader->SetSignalWeightExpression("weight");
        m_dataloader->SetBackgroundWeightExpression("weight"); 

    }

    void LQ3btauBDT::PrepareTrainingAndTestTree()
    {
        // --- (5) Prepare training and test trees
        /* Preparation of the internal TMVA training and test trees.
         *  */
        m_dataloader->PrepareTrainingAndTestTree(
                m_cut_sig, /* signal cuts */
                m_cut_bkg, /* backgruond cuts */
                "NormMode=EqualNumEvents:!V"
                );
    }

    void LQ3btauBDT::BookMethod()
    {
        std::cout << "Weight file : " << m_output_weight << std::endl;
        std::cout << "BDT options : " << m_bdt_option    << std::endl;

        // --- (6) Book method 
        m_factory->BookMethod( 
                m_dataloader,
                TMVA::Types::kBDT,  /* Method name */
                m_output_weight,  /* Output weight file */
                m_bdt_option
                );
    }

    void LQ3btauBDT::Executing()
    {
        // --- (7) Executing the MVA training
        std::cout << "==> TrainAllMethods() " << std::endl;
        m_factory->TrainAllMethods(); 

        // --- (8) Executing the validation of the MVA methods
        std::cout << "==> TestAllMethods() " << std::endl;
        m_factory->TestAllMethods();  

        // --- (9) Executing the performance evaluation
        std::cout << "==> EvaluateAllMethods() " << std::endl;
        m_factory->EvaluateAllMethods();

    }
}
