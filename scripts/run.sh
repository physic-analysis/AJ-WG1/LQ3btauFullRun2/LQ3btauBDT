
# 300 GeV
#RunBDT PrevConfig/m300GeV_PrevConfig_wZMassLowerCut.xml             > m300GeV_PrevConfig_wZMassLowerCut.txt            
#RunBDT PrevConfig/m300GeV_PrevConfig_woZMassLowerCut.xml            > m300GeV_PrevConfig_woZMassLowerCut.txt           
#RunBDT PrevConfig/m300GeV_PrevConfig_woZMassLowerCut_wEtmissCut.xml > m300GeV_PrevConfig_woZMassLowerCut_wEtmissCut.txt
#RunBDT NewConfig/m300GeV_NewConfig_woZMassLowerCut.xml              > m300GeV_NewConfig_woZMassLowerCut.txt
#RunBDT NewConfig/m300GeV_NewConfig_woZMassLowerCut_wEtmissCut.xml   > m300GeV_NewConfig_woZMassLowerCut_wEtmissCut.txt
#echo "Finished m300GeV"

# 500 GeV
RunBDT PrevConfig/m500GeV_PrevConfig_wZMassLowerCut.xml             > m500GeV_PrevConfig_wZMassLowerCut.txt            
RunBDT PrevConfig/m500GeV_PrevConfig_woZMassLowerCut.xml            > m500GeV_PrevConfig_woZMassLowerCut.txt           
RunBDT PrevConfig/m500GeV_PrevConfig_woZMassLowerCut_wEtmissCut.xml > m500GeV_PrevConfig_woZMassLowerCut_wEtmissCut.txt
RunBDT NewConfig/m500GeV_NewConfig_woZMassLowerCut.xml              > m500GeV_NewConfig_woZMassLowerCut.txt
RunBDT NewConfig/m500GeV_NewConfig_woZMassLowerCut_wEtmissCut.xml   > m500GeV_NewConfig_woZMassLowerCut_wEtmissCut.txt
echo "Finished m500GeV"

# 900 GeV
RunBDT PrevConfig/m900GeV_PrevConfig_wZMassLowerCut.xml             > m900GeV_PrevConfig_wZMassLowerCut.txt            
RunBDT PrevConfig/m900GeV_PrevConfig_woZMassLowerCut.xml            > m900GeV_PrevConfig_woZMassLowerCut.txt           
RunBDT PrevConfig/m900GeV_PrevConfig_woZMassLowerCut_wEtmissCut.xml > m900GeV_PrevConfig_woZMassLowerCut_wEtmissCut.txt
RunBDT NewConfig/m900GeV_NewConfig_woZMassLowerCut.xml              > m900GeV_NewConfig_woZMassLowerCut.txt
RunBDT NewConfig/m900GeV_NewConfig_woZMassLowerCut_wEtmissCut.xml   > m900GeV_NewConfig_woZMassLowerCut_wEtmissCut.txt
echo "Finished m900GeV"

# 1300 GeV
RunBDT PrevConfig/m1300GeV_PrevConfig_wZMassLowerCut.xml             > m1300GeV_PrevConfig_wZMassLowerCut.txt            
RunBDT PrevConfig/m1300GeV_PrevConfig_woZMassLowerCut.xml            > m1300GeV_PrevConfig_woZMassLowerCut.txt           
RunBDT PrevConfig/m1300GeV_PrevConfig_woZMassLowerCut_wEtmissCut.xml > m1300GeV_PrevConfig_woZMassLowerCut_wEtmissCut.txt
RunBDT NewConfig/m1300GeV_NewConfig_woZMassLowerCut.xml              > m1300GeV_NewConfig_woZMassLowerCut.txt
RunBDT NewConfig/m1300GeV_NewConfig_woZMassLowerCut_wEtmissCut.xml   > m1300GeV_NewConfig_woZMassLowerCut_wEtmissCut.txt
echo "Finished m1300GeV"

# 1700 GeV
RunBDT PrevConfig/m1700GeV_PrevConfig_wZMassLowerCut.xml             > m1700GeV_PrevConfig_wZMassLowerCut.txt            
RunBDT PrevConfig/m1700GeV_PrevConfig_woZMassLowerCut.xml            > m1700GeV_PrevConfig_woZMassLowerCut.txt           
RunBDT PrevConfig/m1700GeV_PrevConfig_woZMassLowerCut_wEtmissCut.xml > m1700GeV_PrevConfig_woZMassLowerCut_wEtmissCut.txt
RunBDT NewConfig/m1700GeV_NewConfig_woZMassLowerCut.xml              > m1700GeV_NewConfig_woZMassLowerCut.txt
RunBDT NewConfig/m1700GeV_NewConfig_woZMassLowerCut_wEtmissCut.xml   > m1700GeV_NewConfig_woZMassLowerCut_wEtmissCut.txt
echo "Finished m1700GeV"
