import subprocess

Scan_Boost        = ("AdaBoost","RealAdaBoost","Bagging","Grad")
Scan_mass         = ("300", "500", "900", "1300", "1700")

for mass in Scan_mass:
    for BoostType in Scan_Boost:
        subprocess.call(['cp','BdtParamScan.sh', 'BdtParamScan_{}GeV_{}.sh'.format(mass, BoostType)])
        subprocess.call(['chmod','+x', 'BdtParamScan_{}GeV_{}.sh'.format(mass, BoostType)])
