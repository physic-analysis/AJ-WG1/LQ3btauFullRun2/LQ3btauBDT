import subprocess

JobHeader = '''
############################################ 
#
# This is the option file for HTCondor
#
############################################ 

# @@@@@@@@@ universe
# Specifies an HTCondor execution environment
#  vanilla   :  the default and is an execution environment for jobs which do not use HTCondor's
#  standard  :  this job has been re-linked via condor_compile with the HTCondor libraries and therefore supports taking checkpoints and remote system calls.
#  scheduler :  for a job that is to run on the machine where the job is submitted. This universe is intended for a job that acts as a metascheduler and will not be preempted.
#  local     :  for a job that is to run on the machine where the job is submitted. This universe runs the job immediately and will not preempt the job.
#  grid      :  forwards the job to an external job management system. Further specification of the grid universe is done with the grid_resource command.
#  java      :  for programs written to the Java Virtual Machine.
#  vm        :  facilitates the execution of a virtual machine.
#  parallel  :  is for parallel jobs (e.g. MPI) that require multiple machines in order to run.
#  docker    :  runs a docker container as an HTCondor job.
#
universe = vanilla

# @@@@@@@@@ executable
# This is the actual shell script that runs
# executable = /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/scripts/Condor/BdtParamScan_{0}GeV_{1}.sh
executable = /data/data2/zp/ktakeda/BDTParameterScan/BdtParamScan.sh
# The job keeps the environmental variables of the shell from which you submit
getenv = True

#  Setting the priority high
# Priority        = +20

#  Specifies the type of machine.
#Requirements    = ( (Arch == "INTEL" || Arch == "X86_64"))
#  You can also specify the node on which it runs, if you want
#Requirements    = ( (Arch == "INTEL" || Arch == "X86_64") &&  Machine == "ascwrk2.hep.anl.gov")

# The following files will be written out in the directory from which you submit the job
# The next two will be written out at the end of the job; they are stdout and stderr
log    = /data/data2/zp/ktakeda/BDTParameterScan/HTCondorOutput/log/$(Cluster).$(Process).log
output = /data/data2/zp/ktakeda/BDTParameterScan/HTCondorOutput/output/$(Cluster).$(Process).out
error  = /data/data2/zp/ktakeda/BDTParameterScan/HTCondorOutput/error/$(Cluster).$(Process).err

# Ask that you transfer any file that you create in the "top directory" of the job
#should_transfer_files = YES
#when_to_transfer_output = ON_EXIT_OR_EVICT

# @@@@@@@@ Job flavour
#   name        | max duration 
# espresso      |   20min   
# microcentury  |   1h      
# longlunch     |   2h      
# workday       |   8h      
# tomorrow      |   1d      
# testmatch     |   3d      
# nextweek      |   1w      
#
#+JobFlavour = "testmatch"
+JobFlavour = "longlunch"

# Job arguments
'''

#print JobHeader

counter=0

Scan_Boost        = ("AdaBoost","RealAdaBoost","Bagging","Grad")
Scan_mass         = ("300", "500", "900", "1300", "1700")
Scan_NTrees       = ("100","500","800","2000")
Scan_MaxDepth     = (  "3",  "4", "10", "50")
Scan_MinNodeSize  = (  "1",  "5", "10")
Scan_nCuts        = (  "5", "10","100","500")

for mass in Scan_mass:
    for BoostType in Scan_Boost:
        arguments = ""

        for NTrees in Scan_NTrees:
            for MaxDepth in Scan_MaxDepth:
                for MinNodeSize in Scan_MinNodeSize:
                    for nCuts in Scan_nCuts:  
    
                        # NTrees       = 100 + i_NTrees*50
                        # MaxDepth     = 1   + i_MaxDepth
                        # MinNodeSize  = 1   + i_MinNodeSize
                        # nCuts        = 20  + i_nCuts*10
                        counter +=1
                        arguments += "arguments = NewConfig/m" + mass + "GeV_NewConfig_woZMassLowerCut_wEtmissCut.xml " + NTrees \
                                + " " + MaxDepth + " " + MinNodeSize + " " + nCuts + " " + BoostType + "\n"
                        arguments += "queue\n"
#        print arguments
        with open('Condor_{}GeV_{}.sub'.format(mass, BoostType), 'w') as f :
            f.write(JobHeader.format(mass,BoostType) + arguments)
            f.close()

