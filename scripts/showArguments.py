
import subprocess

counter=0

Scan_mass         = (300, 500, 900, 1300)
Scan_NTrees       = (100,500,800,2000)
Scan_MaxDepth     = (  3,  4, 10, 50)
Scan_MinNodeSize  = (  1,  5, 10)
Scan_nCuts        = (  5, 10,100,500)
Scan_AdaBoostBeta = (0.15,)

for mass in Scan_mass:
    for NTrees in Scan_NTrees:
        for MaxDepth in Scan_MaxDepth:
            for MinNodeSize in Scan_MinNodeSize:
                for nCuts in Scan_nCuts:  
                    for AdaBoostBeta in Scan_AdaBoostBeta:  
    
                        # NTrees       = 100 + i_NTrees*50
                        # MaxDepth     = 1   + i_MaxDepth
                        # MinNodeSize  = 1   + i_MinNodeSize
                        # nCuts        = 20  + i_nCuts*10
                        counter +=1
                        print "arguments = NewConfig/m" + str(mass) + "GeV_NewConfig_woZMassLowerCut_wEtmissCut.xml " + str(NTrees) + " " + str(MaxDepth) + " " + str(MinNodeSize) + " " + str(nCuts) + " " + str(AdaBoostBeta) + " " + str(mass)
                        print "queue"
                        #subprocess.call(["RunBDTParameterScan", "NewConfig/m1700GeV_NewConfig_woZMassLowerCut_wEtmissCut.xml", str(NTrees), str(MaxDepth), str(MinNodeSize), str(nCuts), str(AdaBoostBeta)])
                        #subprocess.Popen(["RunBDTParameterScan", "NewConfig/m1700GeV_NewConfig_woZMassLowerCut_wEtmissCut.xml", str(NTrees), str(MaxDepth), str(MinNodeSize), str(nCuts), str(AdaBoostBeta)])
    #print counter
