
SRC="/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LeptoQuarkAnalysis/BDTTraining/output/weights_BDT_LQ_AdaBoost_5Vars_0of2/"
DST="/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/source/HbbMVA/data/"

mv ${SRC}/LQ1300_NewCutFlow_BDT_1tag_A.weights.xml ${DST}/LQ1300_NewCutFlow_BDT_1tag_A.weights.xml 
mv ${SRC}/LQ1300_NewCutFlow_BDT_1tag_B.weights.xml ${DST}/LQ1300_NewCutFlow_BDT_1tag_B.weights.xml 
mv ${SRC}/LQ1300_NewCutFlow_BDT_2tag_A.weights.xml ${DST}/LQ1300_NewCutFlow_BDT_2tag_A.weights.xml 
mv ${SRC}/LQ1300_NewCutFlow_BDT_2tag_B.weights.xml ${DST}/LQ1300_NewCutFlow_BDT_2tag_B.weights.xml 
mv ${SRC}/LQ1700_NewCutFlow_BDT_1tag_A.weights.xml ${DST}/LQ1700_NewCutFlow_BDT_1tag_A.weights.xml 
mv ${SRC}/LQ1700_NewCutFlow_BDT_1tag_B.weights.xml ${DST}/LQ1700_NewCutFlow_BDT_1tag_B.weights.xml 
mv ${SRC}/LQ1700_NewCutFlow_BDT_2tag_A.weights.xml ${DST}/LQ1700_NewCutFlow_BDT_2tag_A.weights.xml 
mv ${SRC}/LQ1700_NewCutFlow_BDT_2tag_B.weights.xml ${DST}/LQ1700_NewCutFlow_BDT_2tag_B.weights.xml 
mv ${SRC}/LQ300_NewCutFlow_BDT_1tag_A.weights.xml  ${DST}/LQ300_NewCutFlow_BDT_1tag_A.weights.xml  
mv ${SRC}/LQ300_NewCutFlow_BDT_1tag_B.weights.xml  ${DST}/LQ300_NewCutFlow_BDT_1tag_B.weights.xml  
mv ${SRC}/LQ300_NewCutFlow_BDT_2tag_A.weights.xml  ${DST}/LQ300_NewCutFlow_BDT_2tag_A.weights.xml  
mv ${SRC}/LQ300_NewCutFlow_BDT_2tag_B.weights.xml  ${DST}/LQ300_NewCutFlow_BDT_2tag_B.weights.xml  
mv ${SRC}/LQ500_NewCutFlow_BDT_1tag_A.weights.xml  ${DST}/LQ500_NewCutFlow_BDT_1tag_A.weights.xml  
mv ${SRC}/LQ500_NewCutFlow_BDT_1tag_B.weights.xml  ${DST}/LQ500_NewCutFlow_BDT_1tag_B.weights.xml  
mv ${SRC}/LQ500_NewCutFlow_BDT_2tag_A.weights.xml  ${DST}/LQ500_NewCutFlow_BDT_2tag_A.weights.xml  
mv ${SRC}/LQ500_NewCutFlow_BDT_2tag_B.weights.xml  ${DST}/LQ500_NewCutFlow_BDT_2tag_B.weights.xml  
mv ${SRC}/LQ900_NewCutFlow_BDT_1tag_A.weights.xml  ${DST}/LQ900_NewCutFlow_BDT_1tag_A.weights.xml  
mv ${SRC}/LQ900_NewCutFlow_BDT_1tag_B.weights.xml  ${DST}/LQ900_NewCutFlow_BDT_1tag_B.weights.xml  
mv ${SRC}/LQ900_NewCutFlow_BDT_2tag_A.weights.xml  ${DST}/LQ900_NewCutFlow_BDT_2tag_A.weights.xml  
mv ${SRC}/LQ900_NewCutFlow_BDT_2tag_B.weights.xml  ${DST}/LQ900_NewCutFlow_BDT_2tag_B.weights.xml  

