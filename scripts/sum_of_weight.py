
import ROOT as R

# 1300
mass_list = [300,400,500,600,700,800,900,950,1000,1050,1100,1150,1200,1250,1350,1400,1450,1500,1550,1600,1700,1800,1900,2000]
 
# Process line to skim the TTree
R.gROOT.ProcessLine('\
    struct MyStruct{\
    Float_t weight;\
    Float_t Btag;\
    Float_t isOdd;\
    Float_t Mtautau;\
    Float_t sT;\
    Float_t TauPt;\
    Float_t METCentrality;\
    Float_t dPhiLep0MET;\
    Float_t dRLepJet;\
    Float_t M_LQlep;\
    Float_t M_LQhad;\
    Float_t M_LQlep_CollinearApproximation_min_dM;\
    Float_t M_LQhad_CollinearApproximation_min_dM;\
};')
struct = R.MyStruct()

def fill_tree(in_tree, out_tree, factor = 1):
  """
  Fill the new variable to the mixed file
  """
  for e in in_tree:
    struct.weight        = e.weight * factor
    struct.Btag          = e.Btag
    struct.Mtautau       = e.Mtautau
    struct.isOdd         = e.isOdd
    struct.sT            = e.sT           
    struct.TauPt         = e.TauPt        
    struct.METCentrality = e.METCentrality
    struct.dPhiLep0MET   = e.dPhiLep0MET  
    struct.dRLepJet      = e.dRLepJet     
    struct.M_LQlep       = e.M_LQlep      
    struct.M_LQhad       = e.M_LQhad      
    struct.M_LQlep_CollinearApproximation_min_dM = e.M_LQlep_CollinearApproximation_min_dM
    struct.M_LQhad_CollinearApproximation_min_dM = e.M_LQhad_CollinearApproximation_min_dM
    out_tree.Fill()

for mc_campaign in ("mc16a", "mc16d", "mc16e"):

  for index, mass in enumerate(mass_list):
    print("--------- M{} GeV --------- ".format(mass))
    input_file  = R.TFile.Open("{}/SR/Nominal/MVAInput_{}.TauLH.LQ3.M{}.root".format(mc_campaign, mc_campaign, mass))
    # Central mass
    tree = input_file.Get("LQ3Up{}".format(mass))
    
    # Output file
    output_file = R.TFile.Open("mixed_{}.LQ3.M{}.root".format(mc_campaign, mass), "RECREATE")
    output_tree = R.TTree("LQ3Up{}".format(mass), '')
    output_tree.Branch('weight',                                R.AddressOf(struct, 'weight'                               ), 'weight/F')
    output_tree.Branch('Btag',                                  R.AddressOf(struct, 'Btag'                                 ), 'Btag/F')
    output_tree.Branch('Mtautau',                               R.AddressOf(struct, 'Mtautau'                              ), 'Mtautau/F')
    output_tree.Branch('isOdd',                                 R.AddressOf(struct, 'isOdd'                                ), 'isOdd/F')
    output_tree.Branch('sT',                                    R.AddressOf(struct, 'sT',                                  ), 'sT/F')
    output_tree.Branch('TauPt',                                 R.AddressOf(struct, 'TauPt',                               ), 'TauPt/F') 
    output_tree.Branch('METCentrality',                         R.AddressOf(struct, 'METCentrality',                       ), 'METCentrality/F')
    output_tree.Branch('dPhiLep0MET',                           R.AddressOf(struct, 'dPhiLep0MET',                         ), 'dPhiLep0MET/F')
    output_tree.Branch('dRLepJet',                              R.AddressOf(struct, 'dRLepJet',                            ), 'dRLepJet/F')          
    output_tree.Branch('M_LQlep',                               R.AddressOf(struct, 'M_LQlep',                             ), 'M_LQlep/F')         
    output_tree.Branch('M_LQhad',                               R.AddressOf(struct, 'M_LQhad',                             ), 'M_LQhad/F')        
    output_tree.Branch('M_LQlep_CollinearApproximation_min_dM', R.AddressOf(struct, 'M_LQlep_CollinearApproximation_min_dM'), 'M_LQlep_CollinearApproximation_min_dM/F')
    output_tree.Branch('M_LQhad_CollinearApproximation_min_dM', R.AddressOf(struct, 'M_LQhad_CollinearApproximation_min_dM'), 'M_LQhad_CollinearApproximation_min_dM/F')
    
    sum_of_weight = 0
    for e in tree:
      sum_of_weight+= e.weight
    
    fill_tree( tree, output_tree )
    
    print("cernter = {}".format(sum_of_weight))
  
    # Weight factor calculation
    # Left mass
    if index is not 0 : 
      input_file_left = R.TFile.Open("{}/SR/Nominal/MVAInput_{}.TauLH.LQ3.M{}.root".format(mc_campaign, mc_campaign, mass_list[index-1]))
      tree_left   = input_file_left.Get("LQ3Up{}".format(mass_list[index-1]))
      sum_of_weight_left = 0
      for e in tree_left:
        sum_of_weight_left += e.weight
      
      factor = sum_of_weight/sum_of_weight_left
      print("left = {}, factor = {}".format(sum_of_weight_left, factor))
    
      fill_tree( tree_left, output_tree, factor )
  
    # Right mass
    if index is not len(mass_list)-1:
      input_file_right = R.TFile.Open("{}/SR/Nominal/MVAInput_{}.TauLH.LQ3.M{}.root".format(mc_campaign, mc_campaign, mass_list[index+1]))
      tree_right = input_file_right.Get("LQ3Up{}".format(mass_list[index+1]))
      
      sum_of_weight_right = 0
      for e in tree_right:
        sum_of_weight_right += e.weight
      
      factor = sum_of_weight/sum_of_weight_right
      print("right = {}, factor = {}".format(sum_of_weight_right, factor))
      
      fill_tree( tree_right, output_tree, factor )
    
    output_file.Write()
    output_file.Close()

