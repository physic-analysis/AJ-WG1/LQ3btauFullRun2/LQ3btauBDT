import subprocess

for mass in ("300", "500", "900", "1300", "1700"):
    for btag in ("1btag", "2btag"):
        for OddEven in ("A", "B"):
            for Cut in ("NewVarSet_woEtmissCut","NewVarSet_wEtmissCut","wZMassLowerCut", "woZMassLowerCut_wEtmissCut","woZMassLowerCut" ):
                file_name = "LQ" + mass + "_BDT_" + btag + "_" + OddEven + "_" + Cut +  ".weights.xml"
                source_dir = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/output/weights_BDT_LQ_AdaBoost_5Vars_0of2/"
                target_dir = "/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/source/HbbMVA/data"

                subprocess.call(["cp", source_dir + file_name, target_dir])
