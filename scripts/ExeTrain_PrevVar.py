import subprocess 

for iOddEvent in ("A", "B"):
    for iMassPoint in ('300', '500', '900', '1300', '1700'):
        for iBtag in ('1','2'):
            ## signal file name
            signal = ""
            if   iMassPoint == '300'  : signal = "testNtup.TauLH.LQBeta05.group.phys-higgs.18276062._000001.CxAOD.root.0.root"
            elif iMassPoint == '500'  : signal = "testNtup.TauLH.LQBeta05.group.phys-higgs.18276064._000001.CxAOD.root.0.root"
            elif iMassPoint == '900'  : signal = "testNtup.TauLH.LQBeta05.group.phys-higgs.18276066._000001.CxAOD.root.0.root"
            elif iMassPoint == '1300' : signal = "testNtup.TauLH.LQBeta05.group.phys-higgs.18276069._000001.CxAOD.root.0.root"
            elif iMassPoint == '1700' : signal = "testNtup.TauLH.LQBeta05.group.phys-higgs.18276071._000001.CxAOD.root.0.root"
    
            if iMassPoint != '900':
                continue
    
            ## isMet1stQuadrant
            AdditionalCut = "isMet1stQuadrant == 1"
    
            argument = 'TMVAClassification.C("{}", "{}", "{}", "{}", "{}", "testNtup.TauLH.ttbar.410470.0.root", true)'.format(iOddEvent, iMassPoint, iBtag, AdditionalCut, signal)
            command = ["root","-l", "-b", "-q", argument]
            subprocess.call(command)
