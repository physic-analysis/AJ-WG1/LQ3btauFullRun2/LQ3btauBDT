
#include "LQ3btauBDT.h"
#include "XMLReader.h"

int main(int argc, char* argv[])
{
    if ( argc < 2 ) {
        std::cerr << "Please put the correct arguments." << std::endl;
        return 0;
    }

    // Redirect the cout & cerr to the text file.
    std::streambuf *coutbuf = std::cout.rdbuf(); 

    // BDT configuration 
    std::map<TString, TString> bdt_config;
    bdt_config["NTrees"]      = argv[2];
    bdt_config["MaxDepth"]    = argv[3];
    bdt_config["MinNodeSize"] = argv[4];
    bdt_config["nCuts"]       = argv[5];
    bdt_config["BoostType"]   = argv[6];
    std::cout << bdt_config["BoostType"]<< std::endl;
    
    //TString log_file = argv[1];
    //log_file.ReplaceAll("xml","");
    //log_file += "NTrees" + bdt_config["NTrees"] + "_MaxDepth" + bdt_config["MaxDepth"] + "_MinNodeSize" + bdt_config["MinNodeSize"]+ "_nCuts" + bdt_config["nCuts"] + "_AdaBoostBeta" +  bdt_config["AdaBoostBeta"] + ".out";
    //std::ofstream ofs(log_file);
    //std::cout.rdbuf(ofs.rdbuf());


    LQ::LQ3btauBDT bdt(argv[1], bdt_config);
    bdt.RunBDT();

    //std::cout.rdbuf(coutbuf); 
    //ofs.close();
    //std::cout << " -- F I N I S H E D -- " << std::endl;

    return 0;
}
