
#include "LQ3btauBDT.h"
#include "XMLReader.h"
//#include "TVersionCheck.h"

int main(int argc, char* argv[])
{
    if ( argc < 2 ) {
        std::cerr << "Please put the correct arguments." << std::endl;
        return 0;
    }
    /* Argument list
     *  RunBDT config.xml 300
     * [1] : config.xml
     * [2] : mc16a/mc16d/mc16e 
     * [3] : <mass_point> 
     * [4] : 1btag/2btag
     * [5] : A/B 
     * [6] : <Selection name>
     * */
     
    // Redirect the cout & cerr to the text file.
    LQ::LQ3btauBDT bdt(argv[1], argv[2], argv[3], argv[4], argv[5], argv[6]);
    bdt.RunBDT();
    
    std::cout << " -- F I N I S H E D -- " << std::endl;

    return 0;
}
