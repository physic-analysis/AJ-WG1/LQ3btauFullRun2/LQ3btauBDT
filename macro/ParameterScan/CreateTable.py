import subprocess
import pandas as pd

#masses = ('300','500','900','1300','1700')
masses = ('1300',)

for mass in masses:
    for BoostType in ("AdaBoost","RealAdaBoost","Bagging","Grad"):
        full_path = '/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/macro/ParameterScan/ParameterScanTable.cxx("{}")'
        subprocess.call(['root','-l','-b','-q',full_path.format(mass)])
    
        # Nominal
        df    = pd.read_csv('HyperParameter_' + mass + '_' + BoostType + '.csv')
        
        # Sorted by the eff_bkg
        df_sort_bkgrej = df.sort_values('Background rejection eff', ascending=False)
        
        # Sorted by the S/sqrt(B) 
        df_sort_soverb = df.sort_values('S/Sqrt(B)', ascending=False)
        
        pd.set_option('colheader_justify', 'center')
        
        html_string = '''
        <html>
            <head><meta charset="UTF-8">
            <title>Hyperparameter Ranking</title>
            </head>
            <style>
                {style}
            </style>
            <body>
                  {table}
            </body>
        </html>.
        '''
        
        style_string = '''
        .table_style { font-size: 11pt; font-family: Arial; border-collapse: collapse; border: 1px solid silver; }
        .table_style td, th { padding: 5px; } 
        .table_style thead th { position: -webkit-sticky; position: sticky; top: 0;  background-color: #666666;  color: white;  }
        .table_style tr:hover { background: silver; cursor: pointer; }
        
        '''
        
        with open('HyperparameterRanking_'+ mass + '_' + BoostType + '_' + 'nominal.html', 'w') as f:
            f.write(html_string.format(style=style_string,table=df.to_html(classes='table_style', index=False)))
        
        with open('HyperparameterRanking_'+ mass + '_' + BoostType + '_'  +'bkgrej.html', 'w') as f:
            f.write(html_string.format(style=style_string,table=df_sort_bkgrej.to_html(classes='table_style', index=False)))
        
        with open('HyperparameterRanking_'+ mass + '_' + BoostType + '_'  +'soverb.html', 'w') as f:
            f.write(html_string.format(style=style_string,table=df_sort_soverb.to_html(classes='table_style', index=False)))
        
