
void ParameterScanTable(TString mass)
{
    std::vector<TString> Scan_Boost       = { "AdaBoost","RealAdaBoost","Bagging","Grad"};
    std::vector<TString> Scan_NTrees      = { "100","500","800","2000"};
    std::vector<TString> Scan_MaxDepth    = {   "3",  "4", "10", "50"};
    std::vector<TString> Scan_MinNodeSize = {   "1",  "5", "10"};
    std::vector<TString> Scan_nCuts       = {   "5", "10","100","500"};


    for ( int iBoostType=0; iBoostType<Scan_Boost.size(); iBoostType++){
        std::ofstream ofs("HyperParameter_" + mass + "_" + Scan_Boost.at(iBoostType) + ".csv");
        ofs << "nCut,NTree,MaxDepth,MinNodeSize,Background rejection eff,S/Sqrt(B),KS(signal),KS(background)" << "\n";
        for ( int inCuts=0; inCuts < Scan_nCuts.size(); inCuts++ ){
            for ( int iNTree=0; iNTree < Scan_NTrees.size(); iNTree++ ){
                for ( int iMaxDepth=0; iMaxDepth < Scan_MaxDepth.size(); iMaxDepth++ ){
                    for ( int iMinNodeSize=0; iMinNodeSize < Scan_MinNodeSize.size(); iMinNodeSize++ ){

                        TString suffix     = "LQ3Up" + mass + "_NTrees" + Scan_NTrees.at(iNTree) + "_MaxDepth" + Scan_MaxDepth.at(iMaxDepth) + "_MinNodeSize" + Scan_MinNodeSize.at(iMinNodeSize) + "_nCuts" + Scan_nCuts.at(inCuts) + "_" + Scan_Boost.at(iBoostType);
                        TString input_file = "ScanData/" + mass + "GeV/" + Scan_Boost.at(iBoostType) + "/" + suffix + ".root";
                        // Histogram names
                        TString histname_rejBvsS  = "dataset/Method_" + suffix + "/" + suffix + +"/MVA_" + suffix + "_rejBvsS";
                        TString hist_name         = "dataset/Method_" + suffix + "/" + suffix + +"/MVA_" + suffix;

                        TFile* file = new TFile( input_file, "read");
                        if ( !file ) std::cerr << " test error " << std::endl; 

                        TH1D* hist         = (TH1D*)file->Get(histname_rejBvsS);

                        // Get the histograms 
                        //// test 
                        TH1D* hist_S_test        = (TH1D*)file->Get(hist_name + "_S");
                        TH1D* hist_B_test        = (TH1D*)file->Get(hist_name + "_B");
                        TH1D* hist_S_test_high   = (TH1D*)file->Get(hist_name + "_S_high");
                        TH1D* hist_B_test_high   = (TH1D*)file->Get(hist_name + "_B_high");
                        //// train
                        TH1D* hist_S_train       = (TH1D*)file->Get(hist_name + "_Train_S");
                        TH1D* hist_B_train       = (TH1D*)file->Get(hist_name + "_Train_B");
                        TH1D* hist_S_train_high  = (TH1D*)file->Get(hist_name + "_S_high");
                        TH1D* hist_B_train_high  = (TH1D*)file->Get(hist_name + "_B_high");

                        /* Working point finding */
                        TH1D* hist_S_test_high_cuml       = (TH1D*)hist_S_test_high->GetCumulative();
                        hist_S_test_high_cuml->Scale(1/hist_S_test_high_cuml->GetBinContent(hist_S_test_high_cuml->GetNbinsX()));
                        double delta = 999;
                        int WP_bin   = 999;
                        const double WORKING_POINT = 0.5;
                        for (int iBin=1; iBin<hist_S_test_high_cuml->GetNbinsX()+1; iBin++) {
                            if ( fabs( WORKING_POINT - hist_S_test_high_cuml->GetBinContent(iBin)) < delta ) {
                                delta  = fabs( WORKING_POINT - hist_S_test_high_cuml->GetBinContent(iBin));
                                WP_bin = iBin;
                            }
                        }

                        double KSSig           = hist_S_test->KolmogorovTest(hist_S_train, "X");
                        double KSBkg           = hist_B_test->KolmogorovTest(hist_B_train, "X");
                        double BkgRejectionEff = hist->GetBinContent(hist->FindBin(WORKING_POINT));

                        //std::cout << input_file << std::endl;
                        //std::cout << WP_bin << " " << hist_S_test_high->GetBinContent(WP_bin) << " " << hist_B_test_high->GetBinContent(WP_bin) << std::endl;
                        //std::cout << WP_bin << " " << hist_S_test_high->Integral(0,WP_bin) << " " << hist_B_test_high->Integral(0,WP_bin) << " " << hist_S_test_high->Integral(0,WP_bin)/TMath::Sqrt(hist_B_test_high->Integral(0,WP_bin)) << " bkgeff=" << BkgRejectionEff <<  std::endl;
                        double SigOverSqrtB    = hist_S_test_high->Integral(WP_bin,hist_S_test_high->GetNbinsX() )/TMath::Sqrt(hist_B_test_high->Integral(WP_bin, hist_B_test_high->GetNbinsX()));

                        ofs << Scan_nCuts.at(inCuts) << "," << Scan_NTrees.at(iNTree) << "," << Scan_MaxDepth.at(iMaxDepth) << "," << Scan_MinNodeSize.at(iMinNodeSize) << ",";
                        ofs << BkgRejectionEff << "," << SigOverSqrtB << "," << KSSig << "," << KSBkg << "\n";

                    }
                }
            }
        }
        ofs.close();
    }

}
