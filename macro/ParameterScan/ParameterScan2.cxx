
void ParameterScan2()
{

    TH2D* ParaScan = new TH2D("ParaScan","ParaScan", 15,0,15,192,0,192);
    TH2D* BkgEff   = new TH2D("BkgEff"  ,"BkgEff"  , 1, 0, 1,192,0,192);
    TGraph* grBkgEff = new TGraph();

    std::vector<TString> Scan_NTrees      = { "100","500","800","2000"};
    std::vector<TString> Scan_MaxDepth    = {   "3",  "4", "10", "50"};
    std::vector<TString> Scan_MinNodeSize = {   "1",  "5", "10"};
    std::vector<TString> Scan_nCuts       = {   "5", "10","100","500"};

    std::vector<std::string> vecLabel;
    std::vector<double>      bkgEff;

    int yAxisIndex=0;
    for ( int iNTree=0; iNTree < Scan_NTrees.size(); iNTree++ ){
        for ( int iMaxDepth=0; iMaxDepth < Scan_MaxDepth.size(); iMaxDepth++ ){
            for ( int iMinNodeSize=0; iMinNodeSize < Scan_MinNodeSize.size(); iMinNodeSize++ ){
                for ( int inCuts=0; inCuts < Scan_nCuts.size(); inCuts++ ){
                    TString suffix     = "LQ3UpBDT_NTrees" + Scan_NTrees.at(iNTree) + "_MaxDepth" + Scan_MaxDepth.at(iMaxDepth) + "_MinNodeSize" + Scan_MinNodeSize.at(iMinNodeSize) + "_nCuts" + Scan_nCuts.at(inCuts) + "_AdaBoostBeta";
                    TString input_file = "ScanData/" + suffix + ".root";
                    TString hist_name  = "dataset/Method_" + suffix + "/" + suffix + +"/MVA_" + suffix + "_rejBvsS";
                    
                    // std::cout << input_file << std::endl;
                    // std::cout << hist_name << std::endl;
                    TFile* file = new TFile( input_file, "read");
                    if ( !file ) std::cerr << " test error " << std::endl; 
                    
                    TH1D* hist = (TH1D*)file->Get(hist_name);

                    ParaScan->Fill(iNTree                                                                          , yAxisIndex);
                    ParaScan->Fill(Scan_NTrees.size()                                                + iMaxDepth   , yAxisIndex);
                    ParaScan->Fill(Scan_NTrees.size() +Scan_MaxDepth.size()                          + iMinNodeSize, yAxisIndex);
                    ParaScan->Fill(Scan_NTrees.size() +Scan_MaxDepth.size() +Scan_MinNodeSize.size() + inCuts      , yAxisIndex);

                    grBkgEff->SetPoint(grBkgEff->GetN(), yAxisIndex+0.5, hist->GetBinContent(hist->FindBin(0.5)));

                    // std::cout << 1+iNTree                                 << std::endl;
                    // std::cout << 1+Scan_NTrees.size()      + iMaxDepth    << std::endl;
                    // std::cout << 1+Scan_NTrees.size()+Scan_MaxDepth.size()    + iMinNodeSize << std::endl;
                    // std::cout << 1+Scan_NTrees.size()+Scan_MaxDepth.size()+Scan_MinNodeSize.size() + inCuts       << std::endl;
                    std::stringstream ss;
                    ss << Scan_NTrees.at(iNTree) << ", " << Scan_MaxDepth.at(iMaxDepth) << ", " << Scan_MinNodeSize.at(iMinNodeSize) << ", " << Scan_nCuts.at(inCuts);
                    vecLabel.push_back(ss.str());
                    bkgEff.push_back(hist->GetBinContent(hist->FindBin(0.5)));
                    
                    yAxisIndex++;
                }
            }
        }
    }

    TCanvas* c = new TCanvas("c","c",2000, 500); 
//    TPad* pad1 = new TPad("pad1","pad1",  0, 0, 0.8, 1.0);
//    TPad* pad2 = new TPad("pad1","pad1",0.81, 0, 1.0, 1.0);
//    pad1->Draw();
//    pad1->SetRightMargin(0.005);
//    pad2->Draw();
//    
//    pad1->cd();
//    //c->SetGrid();
//    ParaScan->GetXaxis()->SetLabelOffset(1000);
//    ParaScan->GetYaxis()->SetLabelOffset(1000);
//    ParaScan->Draw("col");
//
//    for ( int i=0; i<192; i++ ){
//        TLine* line = new TLine(0,i+1,15,i+1);
//        line->SetLineStyle(1);
//        line->SetLineWidth(1);
//        line->Draw();
//    }
//
//    pad2->cd();

    for ( unsigned int index=0; index<vecLabel.size(); index++){
        grBkgEff->GetXaxis()->SetBinLabel(index+1, vecLabel.at(index).c_str());
//        EColor ec;
//        if      ( bkgEff.at(index) < 0.5 ) ec = kYellow;
//        else if ( bkgEff.at(index) < 0.6 ) ec = kBlue;
//        else if ( bkgEff.at(index) < 0.7 ) ec = kRed;
//        else if ( bkgEff.at(index) < 0.8 ) ec = kMagenta;
//        else if ( bkgEff.at(index) < 0.9 ) ec = kGreen;
//        grBkgEff->GetXaxis()->SetBinLabel(index, vecLabel.at(index).c_str());
    }
    gPad->SetGrid();
    grBkgEff->GetXaxis()->SetLabelSize(0.03);

    grBkgEff->GetXaxis()->SetRangeUser(0,192);
    grBkgEff->GetYaxis()->SetRangeUser(0.5,1.05);
//    grBkgEff->GetHistogram()->GetYaxis()->SetLabelOffset(1000000);
//    //grBkgEff->GetHistogram()->GetXaxis()->SetLabelOffset(1000000);
    grBkgEff->SetMarkerSize(0.7);
    grBkgEff->Draw("");
    //TCanvas* c = new TCanvas("c","c",500, 1000); 
}
