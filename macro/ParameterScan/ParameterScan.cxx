
void ParameterScan(TString mass)
{

    TH2D* ParaScan = new TH2D("ParaScan","ParaScan", 15,0,15,192,0,192);
    TH2D* BkgEff   = new TH2D("BkgEff"  ,"BkgEff"  , 1, 0, 1,192,0,192);
    TGraph* grBkgEff = new TGraph();
    TGraph* grKsS = new TGraph();
    TGraph* grKsB = new TGraph();

    std::vector<TString> Scan_NTrees      = { "100","500","800","2000"};
    std::vector<TString> Scan_MaxDepth    = {   "3",  "4", "10", "50"};
    std::vector<TString> Scan_MinNodeSize = {   "1",  "5", "10"};
    std::vector<TString> Scan_nCuts       = {   "5", "10","100","500"};

    std::vector<double> vecBkgEff;
    std::vector<double> vecKsS;
    std::vector<double> vecKsB;

    int yAxisIndex=0;
    for ( int inCuts=0; inCuts < Scan_nCuts.size(); inCuts++ ){
        for ( int iNTree=0; iNTree < Scan_NTrees.size(); iNTree++ ){
            for ( int iMaxDepth=0; iMaxDepth < Scan_MaxDepth.size(); iMaxDepth++ ){
                for ( int iMinNodeSize=0; iMinNodeSize < Scan_MinNodeSize.size(); iMinNodeSize++ ){
                    TString suffix     = "LQ3Up" + mass + "_NTrees" + Scan_NTrees.at(iNTree) + "_MaxDepth" + Scan_MaxDepth.at(iMaxDepth) + "_MinNodeSize" + Scan_MinNodeSize.at(iMinNodeSize) + "_nCuts" + Scan_nCuts.at(inCuts) + "_AdaBoostBeta0.15";
                    TString input_file = "ScanData/" + suffix + ".root";
                    TString hist_name  = "dataset/Method_" + suffix + "/" + suffix + +"/MVA_" + suffix + "_rejBvsS";

                     std::cout << suffix << " " << input_file << " " << hist_name << std::endl;
                    
                    TString ks_hist_name  = "dataset/Method_" + suffix + "/" + suffix + +"/MVA_" + suffix;

                    TFile* file = new TFile( input_file, "read");
                    if ( !file ) std::cerr << " test error " << std::endl; 

                    TH1D* hist = (TH1D*)file->Get(hist_name);
                    TH1D* ks_hist_S_test  = (TH1D*)file->Get(ks_hist_name + "_S");
                    TH1D* ks_hist_S_train = (TH1D*)file->Get(ks_hist_name + "_Train_S");
                    TH1D* ks_hist_B_test  = (TH1D*)file->Get(ks_hist_name + "_B");
                    TH1D* ks_hist_B_train = (TH1D*)file->Get(ks_hist_name + "_Train_B");
                    
                    vecKsS.push_back( ks_hist_S_test->KolmogorovTest(ks_hist_S_train, "X") );
                    vecKsB.push_back( ks_hist_B_test->KolmogorovTest(ks_hist_B_train, "X") );
                    
                    grKsS->SetPoint( grKsS->GetN(), ks_hist_S_test->KolmogorovTest(ks_hist_S_train, "X"), yAxisIndex+0.5);
                    grKsB->SetPoint( grKsB->GetN(), ks_hist_B_test->KolmogorovTest(ks_hist_B_train, "X"), yAxisIndex+0.5);

                    ParaScan->Fill(inCuts                                                                          , yAxisIndex);
                    ParaScan->Fill(Scan_nCuts.size()                                                + iNTree       , yAxisIndex);
                    ParaScan->Fill(Scan_nCuts.size() +Scan_NTrees.size()                            + iMaxDepth    , yAxisIndex);
                    ParaScan->Fill(Scan_nCuts.size() +Scan_NTrees.size() +Scan_MaxDepth.size()      + iMinNodeSize , yAxisIndex);

                    grBkgEff->SetPoint(grBkgEff->GetN(), hist->GetBinContent(hist->FindBin(0.5)), yAxisIndex+0.5 );
                    yAxisIndex++;

                    // std::cout << 1+iNTree                                 << std::endl;
                    // std::cout << 1+Scan_NTrees.size()      + iMaxDepth    << std::endl;
                    // std::cout << 1+Scan_NTrees.size()+Scan_MaxDepth.size()    + iMinNodeSize << std::endl;
                    // std::cout << 1+Scan_NTrees.size()+Scan_MaxDepth.size()+Scan_MinNodeSize.size() + inCuts       << std::endl;
                    //std::cout << Scan_NTrees.at(iNTree) << " " << Scan_MaxDepth.at(iMaxDepth) << " " << Scan_MinNodeSize.at(iMinNodeSize) << " " << Scan_nCuts.at(inCuts) << " " << hist->GetBinContent(hist->FindBin(0.5)) << std::endl;
                    vecBkgEff.push_back(hist->GetBinContent(hist->FindBin(0.5)));
                }
            }
        }
    }

    TCanvas* c = new TCanvas("c","c",800, 1000); 
    TPad* pad1 = new TPad("pad1","pad1",  0, 0, 0.7, 1.0);
    TPad* pad2 = new TPad("pad2","pad2",0.71, 0, 0.81, 1.0);
    TPad* pad3 = new TPad("pad3","pad3",0.82, 0, 0.92, 1.0);
    pad1->Draw();
    pad1->SetRightMargin(0.005);
    pad2->Draw();
    pad3->Draw();
    
    pad1->cd();
    gStyle->SetPalette(kDeepSea);
    ParaScan->GetYaxis()->SetLabelOffset(1000);
   
    for ( int inCuts=0; inCuts < Scan_nCuts.size(); inCuts++ ){
        ParaScan->GetXaxis()->SetBinLabel(inCuts+1, Scan_nCuts.at(inCuts));
    }
    for ( int iNTree=0; iNTree < Scan_NTrees.size(); iNTree++ ){
        ParaScan->GetXaxis()->SetBinLabel(4+iNTree+1, Scan_NTrees.at(iNTree));
    }
    for ( int iMaxDepth=0; iMaxDepth < Scan_MaxDepth.size(); iMaxDepth++ ){
        ParaScan->GetXaxis()->SetBinLabel(4+4+iMaxDepth+1, Scan_MaxDepth.at(iMaxDepth));
    }
    for ( int iMinNodeSize=0; iMinNodeSize < Scan_MinNodeSize.size(); iMinNodeSize++ ){
        ParaScan->GetXaxis()->SetBinLabel(4+4+4+iMinNodeSize+1, Scan_MinNodeSize.at(iMinNodeSize));
    }
    
    ParaScan->Draw("col");

    pad2->cd();
    grBkgEff->GetXaxis()->SetRangeUser(0,1.05);
    grBkgEff->GetYaxis()->SetRangeUser(0,192);
    grBkgEff->GetHistogram()->GetYaxis()->SetLabelOffset(1000000);
//    grBkgEff->GetHistogram()->GetXaxis()->SetLabelOffset(1000000);
    grBkgEff->SetMarkerSize(0.7);
    grBkgEff->Draw("");

    TLine * line2 = new TLine(0.7, 0, 0.7, 192);
    line2->SetLineStyle(3);
    line2->Draw();
    
    pad3->cd();
    grKsS->GetXaxis()->SetRangeUser(-0.5,1.05);
    grKsS->GetYaxis()->SetRangeUser(0,192);
    grKsS->SetMarkerSize(0.7);
    grKsB->SetMarkerSize(0.7);
    grKsS->SetMarkerColor(kMagenta);
    grKsS->SetLineColor(kMagenta);
    grKsS->GetHistogram()->GetYaxis()->SetLabelOffset(1000000);
    grKsS->GetHistogram()->GetXaxis()->SetLabelOffset(-1);
    grKsS->Draw();
    grKsB->Draw("same PL");

    TLine * line = new TLine(0.5, 0, 0.5, 192);
    line->SetLineStyle(3);
    line->Draw();

    c->SaveAs("c_" + mass + "GeV.pdf");
}
