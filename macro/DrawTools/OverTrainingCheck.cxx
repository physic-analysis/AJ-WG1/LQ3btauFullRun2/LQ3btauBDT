
#include "FileStore.h"
#include "MassStore.h"

void OverTrainingCheck()
{
    for ( int iBtag =0; iBtag<2; iBtag++ ){
        TString btag ="";
        if      ( iBtag == 0 ) btag = "1";
        else if ( iBtag == 1 ) btag = "2";   

        // Store service
        FileStore file_store(btag);
        MassStore mass_store;

        TCanvas* c = new TCanvas("c","c", 2100, 1120);
        c->Divide(3,2);
        gStyle->SetPalette(1);
        EColor ec[5] = {kBlack, kMagenta, kBlue, kGreen, kRed};
        
        for ( int iFileSet=0; iFileSet< file_store.size(); iFileSet++ ){
            TString canvas_name = "";
            for ( size_t iFile=0; iFile<file_store.at(iFileSet).size(); iFile++ ){
                TFile* file = new TFile( "output/" + file_store.at(iFileSet).at(iFile) );
                std::cout << file->GetName() << std::endl;

                // histgram names
                TString histname = file_store.at(iFileSet).at(iFile);
                for ( int imass=0; imass< mass_store.size(); imass++ ){
                    histname.ReplaceAll("LQ" + mass_store.at(imass) + "_", "");
                }
                histname.ReplaceAll(".root", "");
                // Canvas names 
                canvas_name = histname;
                for ( int imass=0; imass< mass_store.size(); imass++ ){
                    canvas_name.ReplaceAll("LQ" + mass_store.at(imass) + "_BDT_"  + btag  + "btag_A", "");
                }

                TH1D* histS       = nullptr;
                TH1D* histB       = nullptr;
                TH1D* histS_Train = nullptr;
                TH1D* histB_Train = nullptr;
                
                histS       = (TH1D*)file->Get("Method_BDT/" + histname + "/MVA_" + histname + "_S");
                histB       = (TH1D*)file->Get("Method_BDT/" + histname + "/MVA_" + histname + "_B");
                histS_Train = (TH1D*)file->Get("Method_BDT/" + histname + "/MVA_" + histname + "_Train_S");
                histB_Train = (TH1D*)file->Get("Method_BDT/" + histname + "/MVA_" + histname + "_Train_B");
                
                c->cd(iFile+1);

                histS->GetYaxis()->SetRangeUser(0.001, histS->GetMaximum()>histB->GetMaximum()?histS->GetMaximum()+1:histB->GetMaximum()+1);
                histS->GetXaxis()->SetTitle("BDT response");
                histS->GetXaxis()->SetTitleOffset(1.2);


                histS->SetFillColor(kAzure+8);
                histS->SetLineColor(kAzure+8);

                histB->SetFillStyle(3335);
                histB->SetLineColor(kRed);
                histB->SetLineWidth(1);
                histB->SetFillColor(kRed);

                histS->Draw("hist");
                histB->Draw("hist same");

                histS_Train->SetMarkerColor(kBlue);
                histS_Train->SetLineColor(kBlue);
                histS_Train->SetLineWidth(1);

                histB_Train->SetMarkerColor(kRed);
                histB_Train->SetLineColor(kRed);
                histB_Train->SetLineWidth(1);

                histS_Train->Draw("same");
                histB_Train->Draw("same");

                TLegend* leg= new TLegend(0.2,0.7,0.6,0.9);
                leg->SetFillStyle(0);
                leg->AddEntry( histS, Form("signal (test) : %.0f",histS->GetEntries()), "f");
                leg->AddEntry( histB, Form("background (test) : %.0f", histB->GetEntries()), "f");
                leg->AddEntry( histS_Train, Form("signal (train) : %.0f", histS_Train->GetEntries()), "lp");
                leg->AddEntry( histB_Train, Form("background (train) :%.0f", histB_Train->GetEntries()), "lp");
                TString mass;
                if      ( iFile == 0 ) mass = "300 GeV";
                else if ( iFile == 1 ) mass = "500 GeV";
                else if ( iFile == 2 ) mass = "900 GeV";
                else if ( iFile == 3 ) mass = "1300 GeV";
                else if ( iFile == 4 ) mass = "1700 GeV";
                leg->AddEntry( (TObject*)0, btag + " b-tag SR (" + mass + ")", "");
                if ( file_store.at(iFileSet).at(iFile).Contains("NewVarSet") ) leg->AddEntry((TObject*)0, canvas_name);
                else                                                           leg->AddEntry((TObject*)0, "Prev. " + canvas_name);
                leg->SetFillColor(0);
                leg->SetBorderSize(0);

                leg->Draw();
            }
            for ( int canv=0; canv<5; canv++) {
                c->cd(canv+1);
                gPad->SetLogy(0);
            }
            c->SaveAs("OverTrainingCheck_" + canvas_name + ".pdf");
            for ( int canv=0; canv<5; canv++) {
                c->cd(canv+1);
                gPad->SetLogy(1);
            }
            c->SaveAs("OverTrainingCheck_" + canvas_name + "_Log.pdf");
        }
    }
}
