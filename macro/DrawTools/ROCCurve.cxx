
#include "FileStore.h"
#include "MassStore.h"

void ROCCurve()
{
    for ( int isZoom=0; isZoom<2; isZoom++ ){
        for ( int iBtag =0; iBtag<2; iBtag++ ){
            TString btag ="";
            if      ( iBtag == 0 ) btag = "1";
            else if ( iBtag == 1 ) btag = "2";   

            // Store service
            FileStore file_store(btag);
            MassStore mass_store;

            TCanvas* c = new TCanvas("c","c", 1500, 1000);
            EColor ec[5] = {kBlack, kMagenta, kBlue, kGreen, kRed};

            if ( isZoom ) c->DrawFrame(0.75, 0.95, 1, 1.001);

            for ( int iFileSet=0; iFileSet< file_store.size(); iFileSet++ ){
                TLegend* leg = new TLegend(0.2,0.3,0.5,0.6);
                leg->SetBorderSize(0);
                leg->SetFillStyle(0);

                for ( size_t iFile=0; iFile<file_store.at(iFileSet).size(); iFile++ ){

                    TFile* file = new TFile( "output/" + file_store.at(iFileSet).at(iFile) );
                    TH1D* hist;

                    TString mass= mass_store.at(iFile) + " GeV";

                    std::cout << "Mass point = " << mass << " [GeV] " << file_store.at(iFileSet).at(iFile) << std::endl;

                    // histgram names
                    TString histname = file_store.at(iFileSet).at(iFile);
                    for ( int imass=0; imass< mass_store.size(); imass++ ){
                        histname.ReplaceAll("LQ" + mass_store.at(imass) + "_", "");
                    }
                    histname.ReplaceAll(".root", "");

                    hist = ((TH1D*)file->Get("Method_BDT/" + histname + "/MVA_" + histname + "_rejBvsS"));

                    // New var
                    if ( iFileSet == 4 ) hist->SetLineStyle(1);

                    double marker_size = 1.3;
                    hist->SetMarkerColor(ec[iFile]);
                    hist->SetMarkerSize(marker_size);

                    if      ( iFileSet == 0 ) { hist->SetMarkerStyle(3); hist->Draw("same p"); }
                    else if ( iFileSet == 1 ) { hist->SetMarkerStyle(24); hist->Draw("same p"); }
                    else if ( iFileSet == 2 ) { hist->SetMarkerStyle(8); hist->Draw("same p"); }
                    else if ( iFileSet == 3 ) { hist->SetMarkerStyle(2); hist->Draw("same p"); }
                    else if ( iFileSet == 4 ) { hist->Draw("same");}

                    hist->SetLineColor(ec[iFile]);
                    hist->GetYaxis()->SetRangeUser(0.95,1.001);
                    leg->AddEntry(hist, mass, "l");
                }
                leg->AddEntry((TObject*)0, btag + " b-tag SR", "");
                leg->Draw();
            }
            if ( isZoom ) { c->RedrawAxis();c->SaveAs("ROCCurve_" + btag + "btag_HighMassRegion.pdf");}
            else            c->SaveAs("ROCCurve_" + btag + "btag.pdf");
        }
    }
}
