
void DrawCorrelation()
{
    for ( int iBtag =0; iBtag<2; iBtag++ ){
        TString btag ="";
        if      ( iBtag == 0 ) btag = "1";
        else if ( iBtag == 1 ) btag = "2";   
        std::vector<std::vector<TString>> file_set =
        {
            {
                "LQ300_BDT.NewCutFlow_" + btag + "btag.weightA.root",
                "LQ500_BDT.NewCutFlow_" + btag + "btag.weightA.root",
                "LQ900_BDT.NewCutFlow_" + btag + "btag.weightA.root",
                "LQ1300_BDT.NewCutFlow_" + btag + "btag.weightA.root",
                "LQ1700_BDT.NewCutFlow_" + btag + "btag.weightA.root",
            },
            {
                "LQ300_BDT.NewCutFlow_" + btag + "btag.weightA_NewVarSet.root",
                "LQ500_BDT.NewCutFlow_" + btag + "btag.weightA_NewVarSet.root",
                "LQ900_BDT.NewCutFlow_" + btag + "btag.weightA_NewVarSet.root",
                "LQ1300_BDT.NewCutFlow_" + btag + "btag.weightA_NewVarSet.root",
                "LQ1700_BDT.NewCutFlow_" + btag + "btag.weightA_NewVarSet.root",
            }
        };

        gStyle->SetPalette(1);
        TCanvas* c = new TCanvas("c","c", 1500, 1000);
        c->Divide(3,2);
        EColor ec[5] = {kBlack, kMagenta, kBlue, kGreen, kRed};
        for ( int iFileSet=0; iFileSet<2; iFileSet++ ){
            for ( size_t iFile=0; iFile<file_set.at(iFileSet).size(); iFile++ ){
                TFile* file = new TFile( "output/" + file_set.at(iFileSet).at(iFile) );
                TH1D* histS = ((TH1D*)file->Get("CorrelationMatrixS"));

                histS->SetMarkerSize(2);
                c->cd(iFile+1);
                histS->Draw("colz text");
            }
            if ( iFileSet == 0 ) c->SaveAs("CorrelationPlots_" + btag + "btag.pdf");
            else                 c->SaveAs("CorrelationPlots_" + btag + "btag_NewVarSet.pdf");
        }
    }
}
