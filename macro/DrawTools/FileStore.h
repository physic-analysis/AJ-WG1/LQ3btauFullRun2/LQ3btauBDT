
class FileStore 
{
    public : 
        FileStore() {};
        FileStore(TString btag);
        ~FileStore() {};
        ssize_t getFileSetSize() { return m_file_set.size();}
        ssize_t getFileSize(int index) { return m_file_set.at(index).size();}
        TString getFileName(int index, int index2) { return m_file_set.at(index).at(index2);}
        ssize_t size() { return m_file_set.size();}
        std::vector<TString> at(int index) { return m_file_set.at(index);}

    private : 
        std::vector<std::vector<TString>> m_file_set;
};

FileStore::FileStore(TString btag)
{
    m_file_set =
    {
        // Old variables w EtmissCut (Full previous results)
        {
            "LQ300_BDT_"  + btag + "btag_A_wZMassLowerCut.root",
            "LQ500_BDT_"  + btag + "btag_A_wZMassLowerCut.root",
            "LQ900_BDT_"  + btag + "btag_A_wZMassLowerCut.root",
            "LQ1300_BDT_" + btag + "btag_A_wZMassLowerCut.root",
            "LQ1700_BDT_" + btag + "btag_A_wZMassLowerCut.root",
        },
        // Old variables w/o EtmissCut
        {
            "LQ300_BDT_"  + btag + "btag_A_woZMassLowerCut.root",
            "LQ500_BDT_"  + btag + "btag_A_woZMassLowerCut.root",
            "LQ900_BDT_"  + btag + "btag_A_woZMassLowerCut.root",
            "LQ1300_BDT_" + btag + "btag_A_woZMassLowerCut.root",
            "LQ1700_BDT_" + btag + "btag_A_woZMassLowerCut.root",
        },
        // Old variables w/o EtmissCut w EtmissCut
        {
            "LQ300_BDT_"  + btag + "btag_A_woZMassLowerCut_wEtmissCut.root",
            "LQ500_BDT_"  + btag + "btag_A_woZMassLowerCut_wEtmissCut.root",
            "LQ900_BDT_"  + btag + "btag_A_woZMassLowerCut_wEtmissCut.root",
            "LQ1300_BDT_" + btag + "btag_A_woZMassLowerCut_wEtmissCut.root",
            "LQ1700_BDT_" + btag + "btag_A_woZMassLowerCut_wEtmissCut.root",
        },
        // New variables w/o EtmissCut
        {
            "LQ300_BDT_"  + btag + "btag_A_NewVarSet_woEtmissCut.root",
            "LQ500_BDT_"  + btag + "btag_A_NewVarSet_woEtmissCut.root",
            "LQ900_BDT_"  + btag + "btag_A_NewVarSet_woEtmissCut.root",
            "LQ1300_BDT_" + btag + "btag_A_NewVarSet_woEtmissCut.root",
            "LQ1700_BDT_" + btag + "btag_A_NewVarSet_woEtmissCut.root",
        },
        // New variables w/ EtmissCut
        {
            "LQ300_BDT_"  + btag + "btag_A_NewVarSet_wEtmissCut.root",
            "LQ500_BDT_"  + btag + "btag_A_NewVarSet_wEtmissCut.root",
            "LQ900_BDT_"  + btag + "btag_A_NewVarSet_wEtmissCut.root",
            "LQ1300_BDT_" + btag + "btag_A_NewVarSet_wEtmissCut.root",
            "LQ1700_BDT_" + btag + "btag_A_NewVarSet_wEtmissCut.root",
        },
    };
}
