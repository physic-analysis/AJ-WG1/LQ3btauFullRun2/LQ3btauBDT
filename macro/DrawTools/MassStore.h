
class MassStore
{
    public :
        MassStore();
        ~MassStore(){};
        ssize_t size() {return m_mass.size();};
        TString at(int index){return m_mass.at(index);};
    private :
        std::vector<TString> m_mass;
};

MassStore::MassStore()
{
    m_mass =
    {
        "300",
        "500",
        "900",
        "1300",
        "1700"
    };
}
