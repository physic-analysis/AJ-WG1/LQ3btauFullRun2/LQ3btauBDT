# How to run the BDT
1. At first you should prepare the config.xml file. This is the options for the TMVA training.
1. Then you can run the TMVA training package using the execution file RunBDT with the correct options.
1. The output file can be found under the your run directory.
1. MIA uses the outputted xml file, thus you need to move these files to the correct directory.

## C++ based BDT training
### Setup
- source setup.sh
    - You can setup the needed gcc version and ROOT version.

- mkdir build
- cd build; cmake ..
- make -j8
    - After the above commands, you can user RunBDT command.

# How to use
## Write the config files
You need to create some config files which are used to give TMVA the correct options. These files should be writtend in XML, and the C++ XML parser 
is used. This implementation can be improved, however anyway the current implementation can work fine.
### Contents
- InputFiles
    - Dir : Input file location
    - Period 
        - Signal (Signal samples)
            - File : File name
            - TTree : TTree name
        - Background (Background samples used for the BDT training)
            - File : File name
            - TTree : TTree name
- Cut
    - Test : Cut used for the test
    - Train : Cut used for the training

- InputVars
    - var : Input variable for the BDT training

- FactoryOptions : TMVA options. Please see the TMVA User Guide.
- BdtOptions : TMVA options. Please see the TMVA User Guide. 

## Preate the mixed MC input samples 
(This BDT training needs the MIA putputted files. Thus if you haven't prepared those yet, you need to run MIA at first.)
After preparing the MVA input files, you should prepare the mixed signal mass samples for each mass point. Our group decided 
to use a signal mass point with the adjavent mass points as the signal sample. Thus you need to do that by the follwing script.
### Mix the signal mass
(This is ktakeda dedicated instruction.)
```
ssh lxatut01
cd /data/data2/zp/ktakeda/MIA/Squirtle
source /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/scripts/change_name.sh
python /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/scripts/sum_of_weight.py
source /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/scripts/ttbar_add.sh
mv mixed* /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/input/mixed
mv MVAInput* /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/input/mixed
```
### Single signal mass
(This is ktakeda dedicated instruction.)
```
ssh lxatut01
cd /data/data2/zp/ktakeda/MIA/Squirtle
source /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/scripts/copy_to_input.sh
source /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/scripts/ttbar_add.sh
mv MVAInput* /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LQ3btauFullRun2/LQ3btauBDT/input/single
exit
source script/change_name_single.sh
```

## Run the TMVA
Please use the run python script.
- python scripts/Local/run.py 

```
cd run
mkdir <Date2020>
python ../../scripts/Local/run.py 
```
### Tips 
The RunBDT command needs an argument of the configuration file names.
- RunBDT config.xml   
    - The config directory should be defined as m_config in the src/LQ3btauBDT.cxx

### Condor execution
You can use HTCondor interface. Please see the scripts/Condor/NominalTrain. There are some condor submit files.

